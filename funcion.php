<?php 
      require "admin/conexion.php";
    function mosMsn($rta){
        switch($rta){
            case "0x001":
                    $msn="<span style=color:red > Nombre imvalido</span>";
                    break;
            case "0x002":
                $msn="<span style=color:red > emal imvalido</span>";
                break;
            case "0x003":
                $msn="<span style=color:red > mensaje imvalido</span>";
                break;
            case "0x004":
                $msn="<span style=color:red >consulta enviado 👍!!</span>";
                break;
            case "0x005":
                $msn="<span style=color:red >e-mail no enviado 😢!!</span>";
                break;
            case "0x006":
                $msn="<span style=color:green >Producto eliminado con Exito 😃👍!!</span>";
                break;
            case "0x007":
                $msn="<span style=color:red >No se borro 😢!!</span>";
                break;
            case "0x008":
                $msn="<span style=color:green > Producto agredado 😃👍!!</span>";
                break;
            case "0x009":
                $msn="<span style=color:red > Nose pudo agregar el producto 😢!!</span>";
                break;
            case "0x010":
                $msn="<span style=color:red > Editado correctamente 👍!!</span>";
                break;
            case "0x011":
                $msn="<span style=color:red >no se Actualizo 😢!!</span>";
                break;
            case "0x012":
                $msn="<span style=color:red >  Estado activado 😃👍!!</span>";
                break;
            case "0x013":
                $msn="<span style=color:red > Estado no funciono  😢👎!!</span>";
                break;
            case "0x014":
                $msn="<span style=color:red > contraseña incorrecto 😃👍!!</span>";
                break;
            case "0x015":
                $msn="<span style=color:red >  registrado coreectamente 😃👍!!</span>";
                break;
            case "0x016":
                $msn="<span style=color:red >  No pudo registrarse  😢👎!!</span>";
                break;
            case "0x017":
                $msn="<span style=color:red > error 17 usuario inactivo , ingrese al correo😢👎!!</span>";
                break;
            case "0x018":
                $msn="<span style=color:red > error 18 datos de acceso incorrecto 😢👎!!</span>";
                break;
            case "0x019":
                $msn="<span style=color:red > error 19 el usuario no existe 😢👎!!</span>";
                break;
            case "0x020":
                $msn="<span style=color:red >  error 20 de sistema  😢👎!!</span>";
                break;
            case "0x021":
                $msn="<span style=color:red >  error 21 no te as logeado 😢👎!!</span>";
                break;
        }
        return $msn;
    }
    function cargarPag($page){
        $page=$page.".php";
        if (file_exists($page)) {
             include($page);
        }else{ include "404.php"; }       
    }
    function mostrarProductos(){        
		$archivo="listadoProductos.csv";
		if($file=fopen($archivo,"r")){
			while(($valor=fgetcsv($file,1000,","))!==false){ 
        ?>
            <div class="product-grid">
                <div class="content_box">
                    <a href="./?page=producto">
                        <div class="left-grid-view grid-view-left">
                            <img src="images/productos/<?php echo $valor[0];?>.jpg" class="img-responsive watch-right" alt=""/>
                        </div>
                    </a>
                    <h4><a href="#"> <?php echo $valor[1];?> </a></h4>
                    <p> <?php echo $valor[5]; ?> </p>
                    <span> <?php echo $valor[2];?> </span>
                </div>
            </div>
        <?php }
			 fclose($file);
		}else{ echo "error"; }
    }
    function borrarProducto($idProducto,$img){
        global $conexion; // la conexion lo hago gloval dentro de la funcion
        /* con variable
        $sql="DELETE FROM productos WHERE idProducto=:id"; // vin para:
        $producto=$conexion->prepare($sql);
        $producto->bindParam(":id",$idProducto,PDO::PARAM_INT);// envia la informacion: nombre de la incognita, el valor y el obJ PDO::tipo_de_dato
        */
        unlink("../imagen/productos/".$img);// borra la imagen de la carpeta
        $sql="DELETE FROM productos WHERE idProducto=?"; // vin para:
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$idProducto,PDO::PARAM_INT);// envia la informacion: nombre de la incognita, el valor y el obJ PDO::tipo_de_dato
        
        if ( $producto->execute() ){
                $rta="0x006";
            }else{
                $rta="0x007";
            }
            return $rta;

    }
    function crearProducto($nombre,$precio,$marca,$categoria,$presentacion,$stock,$imagen){
        global $conexion;
        $imgNombre=$imagen["name"];
        $imgTmp=$imagen["tmp_name"];
        $uploads_dir="../imagen/productos/";
        move_uploaded_file($imgTmp, "$uploads_dir/$imgNombre");
        // $sql="INSERT INTO productos (nombreProd,precio,FKmarca,FKcategoria,presentacion,stock) value(?,?,?,?,?,?)";
        $sql="INSERT INTO productos value(null,?,?,?,?,?,?,?)";
        
        $producto=$conexion->prepare($sql);
            $producto->bindParam(1,$nombre,PDO::PARAM_STR);
            $producto->bindParam(2,$precio,PDO::PARAM_STR);
            $producto->bindParam(3,$marca,PDO::PARAM_INT);
            $producto->bindParam(4,$categoria,PDO::PARAM_INT);
            $producto->bindParam(5,$presentacion,PDO::PARAM_STR);
            $producto->bindParam(6,$stock,PDO::PARAM_INT);
            $producto->bindParam(7,$imgNombre,PDO::PARAM_STR);

            if($producto->execute() ){
                $rta="0x008";
            } else{
                $rta="0x009";
            }
            return $rta;
    }
    function obtenerProducto ($id){
        global $conexion;
        $sql= "SELECT * FROM productos WHERE idProducto=?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$id,PDO::PARAM_INT);// pasamos la info a traves de la info
        if ($producto->execute()){ // lo ejecutamos 
            $producto=$producto->fetch();    // nos permite tomar la info y lo guarda dentro de una variable en formato array
        }
        return $producto;
    }
    function obtenerMarcas ($id){
        global $conexion;
        $sql= "SELECT nombre as marca FROM marcas WHERE idMarcas=?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$id,PDO::PARAM_INT);// pasamos la info a traves de la info
        if ($producto->execute()){ // lo ejecutamos 
            $producto=$producto->fetch();    // nos permite tomar la info y lo guarda dentro de una variable en formato array
        }
        return $producto;
    }
    function obtenerCategoria ($id){
        global $conexion;
        $sql= "SELECT nombre as categoria FROM categorias WHERE idCategoria=?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$id,PDO::PARAM_INT);// pasamos la info a traves de la info
        if ($producto->execute()){ // lo ejecutamos 
            $producto=$producto->fetch();    // nos permite tomar la info y lo guarda dentro de una variable en formato array
        }
        return $producto;
    }
    function actualizarProducto($id,$nombre,$precio,$marca,$categoria,$presentacion,$stock,$imagen,$imgActual){
        global $conexion;
        $imgNombre=$imagen["name"];// nombre de la imagen de BBDD Actual
        if ($imgNombre=="") {
            $imgNombre=$imgActual; // si esta vacio le asigno un nueva img ingresado
        }else {
            $imgTmp=$imagen["tmp_name"];
            $uploads_dir="../imagen/productos/";
            move_uploaded_file($imgTmp, "$uploads_dir/$imgNombre");
            $imgActual=$uploads_dir.$imgActual;
            unlink($imgActual); // borra la imgactual

        }
       
        $sql= "UPDATE productos SET 
        nombreProd =?, precio =?, FKmarca =?, FKcategoria =?, presentacion=?, stock =?, imagen=?  WHERE idProducto =?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$nombre,     PDO::PARAM_STR);// pasamos la info a traves de la info
        $producto->bindParam(2,$precio,     PDO::PARAM_STR);
        $producto->bindParam(3,$marca,      PDO::PARAM_INT);
        $producto->bindParam(4,$categoria,  PDO::PARAM_INT);
        $producto->bindParam(5,$presentacion,PDO::PARAM_STR);
        $producto->bindParam(6,$stock,      PDO::PARAM_INT);
        $producto->bindParam(7,$imgNombre,     PDO::PARAM_STR);
        $producto->bindParam(8,$id,         PDO::PARAM_INT);// pasamos la info a traves de la info

        if($producto->execute()){
            $rta="0x010";
        } else{
            $rta="0x011";
        }
        return $rta;
        
        // hacer un if para confirmar si se actualizo y 
        //agregar los mensajes en mostrarMensajes
    }
    
    /*
    UPDATE productos SET 
    `nombreProd` ="moto Z",
    `precio` =10, 
    `FKmarca` =2, 
    `FKcategoria` =1,
    `presentacion`="8GB",
    `stock` =250, 
    `imagen`=null 
    WHERE `idProducto` =5;
    */
     function estado($idPorducto,$estado){
        global $conexion;
        $sql= "UPDATE productos SET   estado=? WHERE idProducto =?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$estado,     PDO::PARAM_INT);// pasamos la info a traves de la info
        $producto->bindParam(2,$idPorducto, PDO::PARAM_INT);
        if($producto->execute()){
            $rta="0x012";
        } else{
            $rta="0x013";
        }
        return $rta;
        
     }
     function crearUsuario($nombre,$apellido,$usuario,$email,$clave){
         global $conexion;
        //  echo md5($clave); //es pata armar claves no seguras o sha()
        //  $opciones = [
        //     'cost' => 11,
        //     'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        // ];
        //     password_hash($clave,PASSWORD_DEFAULT);
        $clave=password_hash($clave,PASSWORD_DEFAULT);
        $codigo="abcdefghijklmnopqrstuvwz!'#$%yzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $codigos=md5(str_shuffle($codigo));
        // -----  ENVIAR CORREO  -----
            $para="aguilar95giovanny@gmail.com";
            $asunto="activar cuenta";
            $cabecera = "From:" . $para . "\r\n";
            $cabecera.= "MIME-Version: 1.0\r\n";
            $cabecera.= "Content-Type: text/html; charset=UTF-8\r\n";
            $cuerpo.="<img scr='https://www.cronista.com/__export/1596712788672/sites/diarioelcronista/img/2020/08/06/disney-logo_1_crop1596712788219.jpg_1866758231.jpg' style='width:30%' >";
            $cuerpo.="<h1 style=color:pink>Activación de cuenta</h1>";
            $cuerpo.="<b>Click en el siguiente enlace para activar su cuenta</b><br>";
            $cuerpo.="<a style='background-color:lightpink;color:tomato;padding:20px;text-decoration:none;' href=https://miweb.com/activacionDeUsuario.php?correo".$email."&codigo=".$codigo."&estado=1>Activar mi cuenta</a><br><br>Chaito blabla...";  
        
        mail($para,$asunto,$cuerpo,$cabecera);
        $sql="INSERT INTO usuario (nombre,apellido,email,usuario,clave,codigo)
            VALUE (?,?,?,?,?,?)";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$nombre,PDO::PARAM_STR);
        $producto->bindParam(2,$apellido,PDO::PARAM_STR);
        $producto->bindParam(3,$email,PDO::PARAM_STR);
        $producto->bindParam(4,$usuario,PDO::PARAM_STR);
        $producto->bindParam(5,$clave,PDO::PARAM_STR);
        $producto->bindParam(6,$codigo,PDO::PARAM_STR);
        if($producto->execute()){
            $rta="0x015";
        } else{
            $rta="0x016";
        }
        return $rta;
    }
    function aaaccederUsuario($usu,$clave){
        echo "estas en acceder usuario <br>";
        global $conexion;
        $sql="SELECT * FROM usuario WHERE usuario=?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$usu,PDO::PARAM_STR);
           // $producto->fetch(): extraiga lo q ejecuta y guarda en una arrego asociativo
        if($producto->execute()){
            $dato=$producto->fetch();
            echo var_dump($dato);
            if ($dato){
                if ($dato['estado']==0) {
                    header('location:./?page=ingreso&rta=0x017');
                }else{
                    $claveC=$dato['clave'];
                    $usuarioC=$dato['usuario'];
                    echo "//".$clave." === ".$claveC;
                    if($clave==$claveC){
                        if(!password_verify($clave,$claveC)) {echo "okkkkkk";}
                        session_start();
                        $_SESSION['usuario']=$usuarioC;
                        $_SESSION['nombre']=$dato['nombre'];
                        // header("location:./admin/?page=listado");
                    }else{
                        echo "Error -- pas: ".$claveC. " usu: ".$usuarioC;
                        // header('location:./?page=ingreso&rta=0x018');
                    }
                    // session_start(); no debe tener espacion x delante y atras. crea una sesion para gurdar el usuario en la memoria ram y se pueda acceder desde cualquier archivo.
                }
            }else{
                header('location:./?page=ingreso&rta=0x019');
            } 
        } else{
            header('location:./?page=ingreso&rta=0x020');
        }
    }

    //-----------------------------
    function accederUsuario($usuario,$clave){
        global $conexion;
        $sql="SELECT *FROM usuario WHERE usuario=?";
        $producto=$conexion->prepare($sql);
        $producto->bindParam(1,$usuario,PDO::PARAM_STR);
        if($producto->execute()){
            $dato=$producto->fetch();
            if($dato){
                if($dato['estado']==0){
                    header("location:./?page=ingreso&rta=0x017");
                }else{
                    $claveC=$dato['clave'];
                    $usuarioC=$dato['usuario'];
                    echo "//claveIngresado: => ".$clave."==".$claveC."<= claveBBDD";
                    if(password_verify($clave,$claveC)){
                        session_start();//comentarios
                        $_SESSION['usuario']=$usuarioC;
                        $_SESSION['nombre']=$dato['Nombre'];
                        header("location:./admin/?page=listado");
                    }else{
                        // header("location:./?page=ingreso&rta=0x018");
                    }
                }
            }else{
                header("location:./?page=ingreso&rta=0x019");
            }
            
        }else{
            header("location:./?page=ingreso&rta=0x020");
        }
    }

?>